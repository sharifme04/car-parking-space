This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, First install all node modules by `yarn install` and then you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This is the first home view .
![screenshot](screenshots/home.PNG)

After park full.
![screenshot](screenshots/parkfull.PNG)

After during close time.
![screenshot](screenshots/parkclose.PNG)
