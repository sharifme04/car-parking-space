import React, { Component } from 'react';
import './App.css';
import Table from './components/Table';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      count1:0,
      count2:0,
      count3:0,
      park1:9*4,
      park2:9*4,
      park3:9*4,
      today:new Date()
    }
  }

  handlePark(count){
    this.setState(count);
  }

render(){
  const{ park1, park2, park3, count1, count2, count3, today} = this.state;
  let currentSpacePark1 = park1-count1>0?park1-count1: 0;
  let currentSpacePark2 = park2-count2>0?park1-count2: 0;
  let currentSpacePark3 = park3-count3>0?park1-count3: 0;
  let currentSpace;
  let totalAvailableSpace =currentSpacePark1+currentSpacePark2+currentSpacePark3;
  if (totalAvailableSpace === 0) currentSpace  = <p className="close-parking">Parks are full! No Spaces are available now</p>;
  else currentSpace = <Table
                        currentSpacePark1={currentSpacePark1}
                        currentSpacePark2={currentSpacePark2}
                        currentSpacePark3={currentSpacePark3}
                        totalAvailableSpace={totalAvailableSpace}
                      />;
  let hours = today.getHours();

  return (
    <div className="container">
      <h2>Acrontum App (TEST 2)</h2>
      <hr/>
      <h3>Available Space for Car Park</h3>
      <hr/>
      {currentSpace}
      {(hours < 8) || (hours > 20)? <h4 className="close-parking">Currently parking is close</h4>:
        <div className="btn-group">
          <button type="button" onClick={()=>this.handlePark({count1:this.state.count1+1})} className="btn btn-primary">Click if new car come Park1</button>
          <button type="button" onClick={()=>this.handlePark({count2:this.state.count2+1})} className="btn btn-basic">Click if new car come Park2</button>
          <button type="button" onClick={()=>this.handlePark({count3:this.state.count3+1})} className="btn btn-info">Click if new car come Park3</button>
        </div>
        }
    </div>
  );
}
}


export default App;
