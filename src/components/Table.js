import React from 'react';
import PropTypes from 'prop-types';

const Table = (props) => (
  <div>
    <table className="table">
      <thead>
        <tr>
          <th>Car Park 1</th>
          <th>Car Park 2</th>
          <th>Car Park 3</th>
          <th>Total available space </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{props.currentSpacePark1}</td>
          <td>{props.currentSpacePark2}</td>
          <td>{props.currentSpacePark3}</td>
          <td>{props.totalAvailableSpace}</td>
        </tr>
      </tbody>
  </table>
  </div>
);

Table.propTypes = {
  currentSpacePark1:PropTypes.number.isRequired,
  currentSpacePark2:PropTypes.number.isRequired,
  currentSpacePark3:PropTypes.number.isRequired,
  totalAvailableSpace:PropTypes.number.isRequired,
};


export default Table;
